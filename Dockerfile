FROM ruby:3.0.0

RUN mkdir -p /app

COPY . /app

WORKDIR /app

RUN bundle install

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["rails", "s"]
