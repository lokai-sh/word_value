require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MyApplication
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    config.hosts << "word-value-staging.herokuapp.com"
    config.hosts << "word-value.herokuapp.com"
    config.hosts << "3000-jade-cheetah-u0xfzlw2.ws-us18.gitpod.io"
    config.hosts << "3000-blush-pig-2rzb8hot.ws-us18.gitpod.io"
    config.hosts << "3000-violet-toad-be685jv1.ws-us18.gitpod.io"
  end
end
